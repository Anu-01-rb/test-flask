from flask import Flask, request

app = Flask(__name__)

@app.route("/", methods=['PATCH'])
def index():
    return "Hello, Flask!"

@app.route('/post/<string:name>')
def show_post(name):
    return f'hello {name}'

@app.route('/api', methods=['POST'])
def api():
    data = request.json
    return f'Data received successfully! {data["name"]}'

app.run(port=8888, use_reloader=False)